# New Website

Claude Website - Hosted with Firebase Hosting

## Init Firebase Credentials
The first time you setup Firebase with Seethru, you'll need to follow this process, only one time for all the Firebase Functions.a
1. Obtain a JSON service key for the project **seethru-dev**, and copy the key in `~/.ssh/seethru-dev-google-credentials.json`
1. Update your `~/.bash_profile` with
```
export GOOGLE_APPLICATION_CREDENTIALS=~/.ssh/seethru-dev-google-credentials.json
```

## Init this Firebase function
 1. Configure the CLI locally by using `firebase use --add` and select the project **seethru-dev**.
 1. Install dependencies locally by running: `cd app; npm install; cd -`.
 1. Make sure the configuration is available on firebase


## Run locally
 1. Run the web server `firebase serve`

## Deploy
To deploy the function in the dev environment simply run the following:
1. Deploy your project using `firebase deploy --only hosting -P claude-website`
